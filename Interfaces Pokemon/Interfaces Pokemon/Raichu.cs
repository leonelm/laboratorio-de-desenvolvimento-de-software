﻿namespace Interfaces_Pokemon {
    internal class Raichu : IPokemon {
        public IPokemon Evoluir() {
            throw new EPokemonNaoEvolui();
        }

        int[] cor = { 255, 255, 0 };
        public int[] ObterCor() {
            return cor;
        }
    }
}