﻿namespace Interfaces_Pokemon {
internal class Pichu : IPokemon {
    public IPokemon Evoluir() {
        return new Pikachu();
    }

    int[] cor = { 255, 255, 0 };
    public int[] ObterCor() {
        return cor;
    }
}
}