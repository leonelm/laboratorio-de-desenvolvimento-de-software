﻿namespace Interfaces_Pokemon {
interface IPokemon {
    int[] ObterCor();
    IPokemon Evoluir();
}
}