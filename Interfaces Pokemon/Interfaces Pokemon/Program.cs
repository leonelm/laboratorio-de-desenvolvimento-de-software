﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Interfaces_Pokemon {
    class Program {
        static void Main(string[] args) {
            // Para todas as questões as opções são
            // [[1]] = SIM
            // [[2]] = NÃO

            // O2.1 - Recordar que existem interfaces(e a sua sintaxe), que podem ser definidas e implementadas.
            // Questão 1: Dada a interface IPokemon responda as perguntas
            //            interface IPokemon {
            //               float peso;
            //               float ObterPeso();
            //            }
            // Pergunta 1: A implementação a seguir da interface IPokemon pode ter atributos? [[2]]

            // O2.2 - Recordar que as interfaces são implementadas por classes.
            // Questão 2: Uma implementação da interface IPokemon para a classe Pikachu é implementada da seguinte forma:
            //        class Pikachu : IPokemon {
            //        }
            // Já para a classe PikachuShiny as interfaces são implementadas da seguinte forma:
            //        class PikachuShiny : Pikachu, IPokemon, IShiny {
            //        }
            // Pergunta 1: A classe Pikachu tem que implementar todos os métodos da interface IPokemon? [[1]]
            // Pergunta 2: A classe PikachuShiny precisa implmentar todos métodos da interface IPokemon já que herda a classe Pikachu? [[2]]
            // Pergunta 3: Uma vez que a classe PikachuShiny herda a classe Pikachu só precisa implementar os métodos da interface IShiny? [[1]]

            // O2.3 - Recordar que um objeto é uma instanciação de uma classe.
            // Questão: Dada as declarações abaixo responda as perguntas:
            // OBS: A classe Pikachu implementa a interface IPokemon
            // IPokemon pika1 = new Pikachu(); 
            // Pikachu pika2 = new Pikachu();
            // PikachuShiny pikaS = new IShiny();
            // Pergunta 1: Os objetos pika1 e pika2 possuem as mesmas interfaces? [[1]]
            // Pergunta 2: O objeto pikaS pode ser instanciado com a interface IShiny? [[2]]

            // O2.4 - Identificar que o método que envia um objeto para um parâmetro especificado por uma interface não revela outras características do objecto que não as que constam dessa interface.
            // O2.5 - Identificar que se um método receber um objeto apenas por cumprir o que especifica uma interface, esse método não sabe qual a classe desse objeto nem outras características dele que não as que constam dessa interface.
            // Questão 4: Dada a implementação das classes a seguir responda as questões
            //internal interface IShiny {
            //    void AtualizarImagemShiny();
            //    int[] ObterCor();
            //}
            //interface IPokemon {
            //    int[] ObterCor();
            //    IPokemon Evoluir();
            //}
            //class PikachuShiny : Pikachu, IPokemon, IShiny {
            //    public void AtualizarImagemShiny() {
            //        throw new System.NotImplementedException();
            //    }
            //}
            //class Pikachu : IPokemon {
            //            public IPokemon Evoluir() {
            //                return new Raichu();
            //            }
            //            int[] cor = { 255, 255, 0 };
            //            public int[] ObterCor() {
            //                return cor;
            //            }
            //        }
            // Pergunta 1: Dado as instâncias:
            // IPokemon shiny = PikachuShiny();
            // IPokemon pika = Pikachu();
            // A chamada pika.Evoluir() e shiny.Evoluir() retornarão o mesmo um objeto do tipo Raichu? [[1]]
            // Pergunta 2: Ao chamar pika.ObterCor() shiny.ObterCor() estamos chamando a mesma implementação da classe Pikachu? [[1]]
    }
}
}
