﻿namespace Interfaces_Pokemon {
    internal interface IShiny {
        void AtualizarImagemShiny();
        int[] ObterCor();
    }

}