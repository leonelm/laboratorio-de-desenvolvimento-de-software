﻿using System;
using System.Runtime.Serialization;

namespace Interfaces_Pokemon {
    [Serializable]
    internal class EPokemonNaoEvolui : Exception {
        public EPokemonNaoEvolui() {
        }

        public EPokemonNaoEvolui(string message) : base(message) {
        }

        public EPokemonNaoEvolui(string message, Exception innerException) : base(message, innerException) {
        }

        protected EPokemonNaoEvolui(SerializationInfo info, StreamingContext context) : base(info, context) {
        }
    }
}