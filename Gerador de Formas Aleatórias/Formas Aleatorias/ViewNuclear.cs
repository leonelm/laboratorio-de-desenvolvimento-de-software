﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Formas_Aleatorias
{
    namespace View
    {
        public class ViewNuclear
        {
            private Model.ModelNuclear model;
            private FormMain janela;
            private LogView viewlog;

            private List<Model.Forma> listaFormasView;

            public event Action UtilizadorClicouEmNovaForma;
            public event Action UtilizadorClicouEmSair;

            public delegate void SolicitacaoListaFormas(ref List<Model.Forma> listadeformas);
            public event SolicitacaoListaFormas PrecisoDeFormas;

            public delegate List<ILogItem> SolicitacaoLog();
            public event SolicitacaoLog PrecisoDeLog;

            internal ViewNuclear(Model.ModelNuclear m)
            {
                model = m;
                viewlog = new LogView(janela);
            }

            public void Encerrar()
            {
                janela.Encerrar();
            }

            public void AtivarInterface()
            {
                janela = new FormMain();
                janela.View = this;
                // A API WinForms desenha as janelas e botões automaticamente
                janela.ShowDialog();
            }

            public void AtivarViewLog()
            {
                viewlog.AtivarViewDoLog();
            }

            public void AtualizarListaDeFormas()
            {
                // Atualizar a lista de formas recebidas do Model
                Console.WriteLine("A atualizar a lista de formas");
                PrecisoDeFormas(ref listaFormasView);
                DesenharFormas();
            }

            void DesenharFormas()
            {
                //Desenhar as formas no Form
                janela.ConstruirFormas(listaFormasView);
            }

            public void NotificacaoDeLogAlterado()
            {
                string texto="";
                List<ILogItem> itens = PrecisoDeLog();
                foreach (ILogItem item in itens)
                    texto += item.Tipo+": "+item.Mensagem+Environment.NewLine; 

                viewlog.AlterarLog(texto);
            }

            public void CliqueEmNovaForma()
            {
                //Ver comentários em FormMain.cs: botaoNovaForma_Click

                //Note-se que embora a classe View seja informada que a origem é uma classe
                //do mesmo componente (origem) a "origem" que comunica a quem
                //processar este evento (o controller ou o model) é ela própria.
                UtilizadorClicouEmNovaForma();
            }

            //Neste caso, usou-se um método simples para FormMain comunicar com a class View
            //apenas para termos um exemplo de uma forma diferente de atuar do que a usada no botão "Nova forma".
            //Para maior qualidade de homogeneidade, devia ser usada a mesma abordagem.
            public void CliqueEmSair()
            {
                UtilizadorClicouEmSair();
            }
        }
    }
}