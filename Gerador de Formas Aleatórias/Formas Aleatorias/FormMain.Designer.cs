﻿namespace Formas_Aleatorias
{
    namespace View
    {
        partial class FormMain
        {
            /// <summary>
            /// Required designer variable.
            /// </summary>
            private System.ComponentModel.IContainer components = null;

            /// <summary>
            /// Clean up any resources being used.
            /// </summary>
            /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
            protected override void Dispose(bool disposing)
            {
                if (disposing && (components != null))
                {
                    components.Dispose();
                }
                base.Dispose(disposing);
            }

            #region Windows Form Designer generated code

            /// <summary>
            /// Required method for Designer support - do not modify
            /// the contents of this method with the code editor.
            /// </summary>
            private void InitializeComponent()
            {
                this.botaoNovaForma = new System.Windows.Forms.Button();
                this.pictureBox1 = new System.Windows.Forms.PictureBox();
                this.buttonSair = new System.Windows.Forms.Button();
                ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
                this.SuspendLayout();
                // 
                // botaoNovaForma
                // 
                this.botaoNovaForma.Location = new System.Drawing.Point(12, 12);
                this.botaoNovaForma.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
                this.botaoNovaForma.Name = "botaoNovaForma";
                this.botaoNovaForma.Size = new System.Drawing.Size(134, 46);
                this.botaoNovaForma.TabIndex = 0;
                this.botaoNovaForma.Text = "Nova &Forma";
                this.botaoNovaForma.UseVisualStyleBackColor = true;
                this.botaoNovaForma.Click += new System.EventHandler(this.botaoNovaForma_Click);
                // 
                // pictureBox1
                // 
                this.pictureBox1.BackColor = System.Drawing.SystemColors.Window;
                this.pictureBox1.Location = new System.Drawing.Point(152, 2);
                this.pictureBox1.Name = "pictureBox1";
                this.pictureBox1.Size = new System.Drawing.Size(747, 558);
                this.pictureBox1.TabIndex = 1;
                this.pictureBox1.TabStop = false;
                this.pictureBox1.Paint += new System.Windows.Forms.PaintEventHandler(this.pictureBox1_Paint);
                // 
                // buttonSair
                // 
                this.buttonSair.Location = new System.Drawing.Point(12, 118);
                this.buttonSair.Name = "buttonSair";
                this.buttonSair.Size = new System.Drawing.Size(134, 46);
                this.buttonSair.TabIndex = 2;
                this.buttonSair.Text = "Sair";
                this.buttonSair.UseVisualStyleBackColor = true;
                this.buttonSair.Click += new System.EventHandler(this.buttonSair_Click);
                // 
                // FormMain
                // 
                this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
                this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
                this.ClientSize = new System.Drawing.Size(900, 562);
                this.Controls.Add(this.buttonSair);
                this.Controls.Add(this.pictureBox1);
                this.Controls.Add(this.botaoNovaForma);
                this.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
                this.Name = "FormMain";
                this.Text = "Formas";
                ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
                this.ResumeLayout(false);

            }

            #endregion

            private System.Windows.Forms.Button botaoNovaForma;
            private System.Windows.Forms.PictureBox pictureBox1;
            private System.Windows.Forms.Button buttonSair;
        }
    }
}