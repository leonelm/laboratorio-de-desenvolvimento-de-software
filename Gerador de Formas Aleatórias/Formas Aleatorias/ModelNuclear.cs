﻿using System;
using System.Collections.Generic;
using System.Numerics;
using System.Reflection;

namespace Formas_Aleatorias
{
    namespace Model
    {
        class ModelNuclear
        {
            private View.ViewNuclear view;
            List<Forma> formas;

            Random AoCalha;

            public delegate void NotificarListaDeFormasAlteradas();
            public event NotificarListaDeFormasAlteradas ListaDeFormasAlterada;

            Logger modelLog;
            public delegate void NotificacaoLogAlterado();
            public event NotificacaoLogAlterado NotificarLogAlterado;

            public ModelNuclear(View.ViewNuclear v)
            {
                view = v;
                formas = new List<Forma>();
                AoCalha = new Random();
                modelLog = new Logger();
                modelLog.NotificarLogAlterado += LancarNotificarLogAlterado;
            }
            public Logger ModelLog { get => modelLog; set => modelLog = value; }

            public void CriarNovaForma()
            {
                Forma f = new Forma();

                //O parâmetro de Next() é o número de formas possíveis
                int sorteada = AoCalha.Next(Enum.GetNames(typeof(FormasPossiveis)).Length);
                //Isto podia ser feito programaticamente numa linha, mas assim é mais legível.
                switch (sorteada)
                {
                    case 0:
                        f.TipoForma = FormasPossiveis.Retangulo;
                        break;
                    case 1:
                        f.TipoForma = FormasPossiveis.Elipse;
                        break;
                    case 2:
                        f.TipoForma = FormasPossiveis.Estrela;
                        break;
                    case 3:
                        f.TipoForma = FormasPossiveis.Pentagono;
                        break;
                }
                f.Altura = AoCalha.Next(Constantes.EscalaDoMundo);
                f.Largura = AoCalha.Next(Constantes.EscalaDoMundo);
                f.PontoBasilar = new Vector2(AoCalha.Next(Constantes.EscalaDoMundo), AoCalha.Next(Constantes.EscalaDoMundo));

                formas.Add(f);

                // Notifica a que as listas foram alteradas.
                ListaDeFormasAlterada();
            }

            public List<ILogItem> SolicitarLog()
            {
                return modelLog.SolicitarLog();
            }

            private void LancarNotificarLogAlterado()
            {
                NotificarLogAlterado();
            }

            public void RegistarLog(List<FormasPossiveis> formasdesconhecidas)
            {
                ModelLog.RegistarErro(formasdesconhecidas);
            }

            public void SolicitarListaFormas(ref List<Forma> listadeformas)
            {
                // Copia a lista "formas" para "listadeformas"
                // usando o estilo de cópia adequado aos dados
                // (deep copy e não shallow copy).
                // não se deve simplesmente fazer:
                // listadeforma=formas;
                // porque isso permitira que quem manipulasse as formas
                // da lista fornecida alterasse as do próprio Model
                // (visto serem referências).
                listadeformas = new List<Forma>();
                foreach (Forma f in formas)
                {
                    listadeformas.Add(f.Clone());
                }
            }
        }
    }
}