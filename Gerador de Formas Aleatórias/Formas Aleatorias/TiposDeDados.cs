﻿using System.Numerics;

namespace Formas_Aleatorias
{
    namespace Model
    {
        public enum FormasPossiveis { Retangulo, Elipse, Estrela, Pentagono };

        public struct Constantes { public const int EscalaDoMundo = 1000; } ;

        public class Forma
        {
            Vector2 pontobasilar;
            int largura;
            int altura;
            FormasPossiveis tipoForma;

            public FormasPossiveis TipoForma
            {
                get { return tipoForma; }
                set { tipoForma = value; }
            }

            public int Altura
            {
                get { return altura; }
                set { altura = value; }
            }

            public Vector2 PontoBasilar
            {
                get { return pontobasilar; }
                set { pontobasilar = value; }
            }

            public int Largura
            {
                get { return largura; }
                set { largura = value; }
            }

            public Forma Clone()
            {
                Forma f = new Forma();
                f.altura = altura;
                f.largura = largura;
                f.tipoForma = tipoForma;
                f.pontobasilar = new Vector2(this.pontobasilar.X, this.pontobasilar.Y);
                return f;
            }
        }
    }

    public interface ILogItem
    {
        string Tipo { get; }
        string Mensagem { get; }
    }
}