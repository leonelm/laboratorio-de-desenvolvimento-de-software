﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Resources;
using System.Text;
using System.Xml.Schema;

namespace Formas_Aleatorias {
    namespace Model {
        internal class Logger {
            //Podia ser uma classe do Model, mas é apenas para exemplificar que graças às interfaces não é preciso
            //sequer que outras classes do componente (ou de outros componentes) conheçam o tipo de dados concreto,
            //(a classe concreta) que é usada para este efeito.
            //Basta-lhes saber quais os métodos que ela terá, por ter de implementar a interface.
            internal class LogItem : ILogItem {
                private string item = "";
                private char separador = ';';

                public string Tipo {
                    get {
                        string[] partes = item.Split(new[] { separador.ToString() }, StringSplitOptions.None);
                        if (partes.Length < 1)
                            return "Indefinido";
                        else
                            return partes[0];
                    }
                    set {
                        string[] partes = item.Split(new[] { separador.ToString() }, StringSplitOptions.None);
                        if (partes.Length < 2) {
                            Array.Resize(ref partes, 2);
                            partes[1] = "";
                        }
                        partes[0] = value;
                        item = partes[0] + separador + partes[1];
                    }
                }
                public char Separador { get => separador; set => separador = value; }
                public string Mensagem {
                    get {
                        string[] partes = item.Split(new[] { separador.ToString() }, StringSplitOptions.None);
                        if (partes.Length < 2)
                            return "";
                        else
                            return partes[1];
                    }
                    set {
                        string[] partes = item.Split(new[] { separador.ToString() }, StringSplitOptions.None);
                        if (partes.Length < 1) {
                            Array.Resize(ref partes, 2);
                            partes[0] = "Indefinido";
                        }
                        if (partes.Length < 2)
                            Array.Resize(ref partes, 2);
                        partes[1] = value;
                        item = partes[0] + separador + partes[1];
                    }
                }

                public LogItem() { Tipo = "Indefinido"; Mensagem = ""; }
            }


            public event ModelNuclear.NotificacaoLogAlterado NotificarLogAlterado;

            private List<LogItem> log;

            public Logger() {
                log = new List<LogItem>();
            }

            public void RegistarErro(List<FormasPossiveis> formasdesconhecidas) {
                foreach (FormasPossiveis forma in formasdesconhecidas)
                {
                    LogItem item = new LogItem();
                    item.Tipo = "Tipo de forma desconhecido";
                    item.Mensagem = forma.ToString();
                    log.Add(item);
                }
                LogItem fimderegisto = new LogItem();
                fimderegisto.Tipo = "----------------";
                fimderegisto.Mensagem = "----------------";
                log.Add(fimderegisto);

                NotificarLogAlterado();
            }

            public List<ILogItem> SolicitarLog() {
                List<ILogItem> listalog = new List<ILogItem>();
                foreach (ILogItem item in log) {
                    listalog.Add(item);
                }
                return listalog;
            }
        }
    }
}