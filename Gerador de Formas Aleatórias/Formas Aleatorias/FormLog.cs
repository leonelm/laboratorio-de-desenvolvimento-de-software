﻿using System;
using System.Windows.Forms;

namespace Formas_Aleatorias
{
    namespace View
    {
        public partial class FormLog : Form
        {
            public FormLog()
            {
                InitializeComponent();
            }

            public void EscreverLog(string log)
            {
                textBoxLog.Text = log;
            }
        }
    }
}