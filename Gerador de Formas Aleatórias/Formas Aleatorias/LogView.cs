﻿using System;
using System.Windows.Forms;

namespace Formas_Aleatorias
{
    namespace View
    {
        public class LogView
        {
            FormLog formLog;
            Form pai;

            public LogView(Form origem)
            {
                pai = origem;
            }

            internal void AtivarViewDoLog()
            {
                if (formLog == null) formLog = new FormLog();
            }
            internal void AlterarLog(string log)
            {
                if (formLog == null)
                    return;
                formLog.EscreverLog(log);
                formLog.ShowDialog(pai);
            }
        }
    }
}
