﻿using System;
using System.Runtime.Serialization;
using System.Collections.Generic;

namespace Formas_Aleatorias
{
    namespace View
    {
        public class ExceptionFormasDesconhecidas : Exception
        {
            public List<Model.FormasPossiveis> FormasDesconhecidas;

            public ExceptionFormasDesconhecidas()
            {
            }

            public ExceptionFormasDesconhecidas(List<Model.FormasPossiveis> formasdesconhecidas)
            {
                FormasDesconhecidas = formasdesconhecidas;
            }

            public ExceptionFormasDesconhecidas(string message) : base(message)
            {
            }

            public ExceptionFormasDesconhecidas(string message, Exception innerException) : base(message, innerException)
            {
            }

            protected ExceptionFormasDesconhecidas(SerializationInfo info, StreamingContext context) : base(info, context)
            {
            }
        }
    }
}