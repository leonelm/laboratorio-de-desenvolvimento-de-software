﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Windows.Forms;

namespace Formas_Aleatorias
{
    namespace View
    {
        public partial class FormMain : Form
        {
            private ViewNuclear view;
            private List<Model.Forma> listaFormasParaDesenhar = null;

            public FormMain()
            {
                InitializeComponent();
            }

            public void Encerrar()
            {
                Application.Exit();
            }

            public ViewNuclear View { get => view; set => view = value; }

            private void botaoNovaForma_Click(object sender, EventArgs e)
            {
                //O Visual Studio cria automaticamente este método botaoNovaForma_Click com a interface visual
                //Mantendo essa ligação, podemos continuar a usar a edição visual
                //Então comunicamos aqui à nossa classe central do componente View o clique no botão.
                //Podíamos igualmente ir a View.Designer.cs associar diretamente
                //this.botaoNovaForma.Click ao método da classe Controller
                //mas isso desativaria a edição visual
                //e faria com que essa ligação entre classes estivesse a ser feita fora do Controller.
                //Por isso, limitamo-nos a comunicara aqui o evento Click à classe central da View
                //que o relançará como o evento UtilizadorClicouEmNovaForma.
                //Isto permite que seja o Controller a associar o destino dele.
                //Não precisávamos de manter os parâmetros, mantivemo-los apenas
                //por uma questão de enriquecer o exemplo, já que não os usámos noutros eventos
                //deste código exemplificativo.
                view.CliqueEmNovaForma();
            }

            private void buttonSair_Click(object sender, EventArgs e)
            {
                view.CliqueEmSair();
            }

            internal void ConstruirFormas(List<Model.Forma> listaFormasParaDesenhar)
            {
                this.listaFormasParaDesenhar = listaFormasParaDesenhar;
                validarFormas();
                pictureBox1.Invalidate();
            }

            private void validarFormas()
            {
                List<Model.FormasPossiveis> formasdesconhecidas= new List<Model.FormasPossiveis>();

                if (listaFormasParaDesenhar == null) return;
                foreach (Model.Forma f in listaFormasParaDesenhar)
                {
                    switch (f.TipoForma)
                    {
                        case Model.FormasPossiveis.Retangulo:
                            break;
                        case Model.FormasPossiveis.Elipse:
                            break;
                        case Model.FormasPossiveis.Estrela:
                            break;
                        default:
                            formasdesconhecidas.Add(f.TipoForma);
                            break;
                    }
                    if(formasdesconhecidas.Count>0)
                        throw new ExceptionFormasDesconhecidas(formasdesconhecidas);
                }
            }

            private void pictureBox1_Paint(object sender, PaintEventArgs e)
            {
                // Não desenha nada até ter formas na lista para desenhar
                if (listaFormasParaDesenhar == null) return;

                Pen amarela = new Pen(Color.Yellow, 3);
                Pen verde = new Pen(Color.Green, 3);
                Pen vermelha = new Pen(Color.Red, 3);

                foreach (Model.Forma formaParaDesenhar in listaFormasParaDesenhar)
                {
                    formaParaDesenhar.Altura = Convert.ToInt32(pictureBox1.Height / (float)Model.Constantes.EscalaDoMundo * formaParaDesenhar.Altura);
                    formaParaDesenhar.Largura = Convert.ToInt32(pictureBox1.Width / (float)Model.Constantes.EscalaDoMundo * formaParaDesenhar.Largura);
                    formaParaDesenhar.PontoBasilar = new System.Numerics.Vector2(
                                                        pictureBox1.Width / (float)Model.Constantes.EscalaDoMundo * formaParaDesenhar.PontoBasilar.X
                                                        ,
                                                        pictureBox1.Height / (float)Model.Constantes.EscalaDoMundo * formaParaDesenhar.PontoBasilar.Y
                                                        );
                    Point pontodedesenho = new Point(Convert.ToInt32(formaParaDesenhar.PontoBasilar.X), Convert.ToInt32(formaParaDesenhar.PontoBasilar.Y));

                    switch (formaParaDesenhar.TipoForma)
                    {
                        case Model.FormasPossiveis.Retangulo:
                            e.Graphics.DrawRectangle(verde, new Rectangle(pontodedesenho,
                                                                              new Size(formaParaDesenhar.Largura, formaParaDesenhar.Altura)));
                            break;
                        case Model.FormasPossiveis.Elipse:
                            e.Graphics.DrawEllipse(vermelha, new Rectangle(pontodedesenho,
                                                                              new Size(formaParaDesenhar.Largura, formaParaDesenhar.Altura)));
                            break;
                        case Model.FormasPossiveis.Estrela:

                            Point[] pontos = new Point[9];

                            pontos[0] = new Point(pontodedesenho.X, pontodedesenho.Y + formaParaDesenhar.Altura / 2);
                            pontos[0] = new Point(pontodedesenho.X, pontodedesenho.Y + formaParaDesenhar.Altura / 2);
                            pontos[1] = new Point(pontodedesenho.X + 3 * formaParaDesenhar.Largura / 8, pontodedesenho.Y + formaParaDesenhar.Altura / 2 - formaParaDesenhar.Altura / 8);
                            pontos[2] = new Point(pontodedesenho.X + formaParaDesenhar.Largura / 2, pontodedesenho.Y);
                            pontos[3] = new Point(pontodedesenho.X + 5 * formaParaDesenhar.Largura / 8, pontodedesenho.Y + formaParaDesenhar.Altura / 2 - formaParaDesenhar.Altura / 8);
                            pontos[4] = new Point(pontodedesenho.X + formaParaDesenhar.Largura, pontodedesenho.Y + formaParaDesenhar.Altura / 2);
                            pontos[5] = new Point(pontodedesenho.X + 5 * formaParaDesenhar.Largura / 8, pontodedesenho.Y + formaParaDesenhar.Altura / 2 + formaParaDesenhar.Altura / 8);
                            pontos[6] = new Point(pontodedesenho.X + formaParaDesenhar.Largura / 2, pontodedesenho.Y + formaParaDesenhar.Altura);
                            pontos[7] = new Point(pontodedesenho.X + 3 * formaParaDesenhar.Largura / 8, pontodedesenho.Y + formaParaDesenhar.Altura / 2 + formaParaDesenhar.Altura / 8);
                            pontos[8] = pontos[0];

                            e.Graphics.DrawPolygon(amarela, pontos);
                            break;
                    }
                }
            }
        }
    }
}