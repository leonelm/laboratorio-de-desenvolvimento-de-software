﻿using System;
using System.Windows.Forms;

namespace Formas_Aleatorias
{
    namespace Controller
    {
        public class FormasAleatorias
        {
            /// <summary>
            /// Ponto de entrada principal para o aplicativo.
            /// </summary>
            
            static Controller.ControllerNuclear controller;

            [STAThread]
            static void Main()
            {
                //Para ser o nosso código a lidar com as exceções, não o da classe Application.
                Application.SetUnhandledExceptionMode(UnhandledExceptionMode.ThrowException);

                controller = new ControllerNuclear();
                controller.IniciarPrograma();
            }
        }
    }
}