﻿using System;

namespace Quarto_Chines
{
    namespace Controller
    {
        class ControllerNuclear
        {

            private class TextoInput : Model.ITexto
            {
                string texto;
                public string Texto { get => texto; set => texto = value; }
            }

            private Model.ModelNuclear model;
            private View.ViewNuclear view;

            // Eventos e delegados para comunicação 
            public delegate void ApresentacaoTexto();
            public event ApresentacaoTexto Apresentar;

            public delegate void Traducao(Model.ITexto Texto);
            public event Traducao Traduzir;

            public void IniciarPrograma()
            {

                // Inicialização dos componentes.
                view = new View.ViewNuclear();
                model = new Model.ModelNuclear();

                // Vinculação dos eventos entre os componentes

                //No Controller não seria preciso trabalhar
                //com eventos e delegados para os outros componentes,
                //porque o Controller tem sempre de conhecer o fluxo.
                //Contudo, mostra-se aqui como exemplo que
                //até no Controller se pode fazer isso para que a dependência
                //fique apenas neste método de inicialização, pelo que 
                //se a classe for muito extensa se manteve o acoplamento apenas
                //neste método.
                Traduzir += model.Traduzir;

                // Neste caso, estamos apenas a mostrar que se pode associar vários delegados
                // ao mesmo evento. Mas seria preferível ter dois distintos para assegurar a sequência
                // que aqui fica apenas implícita na ordem de associação
                Apresentar += view.ApresentarBoasVindas;
                Apresentar += view.ApresentarRotuloPrompt;

                // Notem que estamos no Controller. A view não sabe que está a ser associado
                // o seu evento PrecisoDaPaginaAtual ao método SolicitarPaginaAtual do model
                // - um exemplo de desacoplamento
                view.PrecisoDaPaginaAtual += model.SolicitarPaginaAtual;

                // Da mesma forma, o Model não quem é que recebe os seus eventos
                model.TraducaoPronta += TraducaoPronta;
                model.PaginaAtualMudou += view.PaginaAtualMudou;
                model.DocumentoTerminou += Encerrar;

                // Apresenta o programa
                Apresentar();

                // Inicia a tradução
                Model.ITexto texto = new TextoInput();
                texto.Texto = DigitarInformacoes();
                view.ApresentarRotuloTraducao();
                try
                {
                    Traduzir(texto);
                }
                catch (Model.ExceptionCaracteresInvalidos ex)
                {
                    view.AvisarTratamentoErro(ex.Carateres);
                    Console.ReadKey();
                    Traduzir(model.CorrigirCarateresInvalidos(texto));
                }
            }

            private void TraducaoPronta(int nPaginas)
            {
                view.MostrarTraducao();
                while (model.EstadoAtualDocumento != false)
                {
                    TeclarQualquer();
                }
            }

            private void Encerrar()
            {
                view.MostrarMSGFinal();
                Console.ReadKey();
                Environment.Exit(0);
            }

            private void TeclarQualquer()
            {
                Console.ReadKey();
                model.AvancarPagina();
            }

            private string DigitarInformacoes()
            {
                return Console.ReadLine();
            }
        }
    }
}