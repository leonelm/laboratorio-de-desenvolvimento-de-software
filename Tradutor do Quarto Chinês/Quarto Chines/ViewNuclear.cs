﻿using System;
using System.Collections.Generic;

namespace Quarto_Chines
{
    namespace View
    {
        public class Pagina:Model.IPagina
        {
            private string texto;
            public string Texto { get => texto; set => texto = value; }
            public Pagina() { Texto = ""; }
        }
        class ViewNuclear
        {

            public delegate int SolicitacaoPaginaAtual(ref Model.IPagina pagina);
            public event SolicitacaoPaginaAtual PrecisoDaPaginaAtual;

            private Model.IPagina pagina;
            private int paginaAtual;

            public ViewNuclear()
            {
                pagina = new Pagina();
            }

            public void ApresentarBoasVindas()
            {
                Console.WriteLine("Bem-vindo ao Tradutor do Quarto Chinês!");
            }

            public void MostrarTraducao()
            {
                //Obtém a página atual
                paginaAtual = PrecisoDaPaginaAtual(ref pagina);
                //Colocá-la no ecrã.
                DesenharPagina();
            }

            private void DesenharPagina()
            {
                Console.WriteLine("Página atual: " + (paginaAtual + 1));
                Console.WriteLine(pagina);
                Console.WriteLine("Prima qualquer tecla para continuar");
            }

            public void ApresentarRotuloPrompt()
            {
                Console.WriteLine("Introduza o texto a traduzir (Ex.: \"Olá\" ou \"Bem-vindo\")");
                Console.Write("> ");
            }

            public void ApresentarRotuloTraducao()
            {
                Console.WriteLine("Texto traduzido:");
            }

            public void PaginaAtualMudou(bool estadoAtualDocumento)
            {
                // Para este exemplo a traducao tem 1 página
                if (estadoAtualDocumento == true) MostrarTraducao();
            }

            public void MostrarMSGFinal()
            {
                Console.WriteLine("Obrigado por utilizar o Tradutor do Quarto Chinês.");
            }

            public void AvisarTratamentoErro(IDictionary<char,int> carateres)
            {
                Console.WriteLine("Há carateres inválidos no texto inserido. Os seguintes carateres não foram considerados no processo de tradução: ");
                foreach (char c in carateres.Keys)
                {
                    Console.Write(c + " ");
                }
                Console.WriteLine("\nPrima qualquer tecla para continuar.");
            }
        }
    }
}