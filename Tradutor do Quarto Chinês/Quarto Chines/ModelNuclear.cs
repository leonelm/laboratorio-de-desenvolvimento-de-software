﻿using System;
using System.Collections.Generic;
using System.Text.RegularExpressions;

namespace Quarto_Chines {
    namespace Model {
        public interface IPagina
        {
            string Texto { get; set; }
        }

        public interface ITexto
        {
            string Texto { get; set; }
        }

        class Pagina : IPagina
        {
            private string texto;
            public string Texto { get => texto; set => texto = value; }
            public Pagina(string t)
            {
                Texto = t;
            }
        }

        class TextoGenerico : ITexto
        {
            private string texto;
            public string Texto { get => texto; set => texto = value; }
            public TextoGenerico(string t)
            {
                Texto = t;
            }
        }

        class ModelNuclear {

            List<IPagina> paginasTraducao;
            int paginaAtual;

            public bool EstadoAtualDocumento { get; private set; }

            private ITexto texto;
            private readonly string listaCaracteresInvalidos = "!¹@²#³$£%¢¬&*()_=+{[ª}]º?/°:;>.<|\"'¨";

            public delegate void EnvioTraducaoPronta(int nPaginas);
            public event EnvioTraducaoPronta TraducaoPronta;

            public delegate void NotificarPaginaMudou(bool estadoAtualDocumento);
            public event NotificarPaginaMudou PaginaAtualMudou;

            public delegate void NotificarOcorrencia();
            public event NotificarOcorrencia DocumentoTerminou;

            public ModelNuclear() {
            }

            public void Traduzir(ITexto t) {
                // Traduzir o texto para chinês e guardá-lo
                // Notifica que a tradução está pronta;
                // Como é um evento, o model não sabe quem o receberá (isso é definido no Controller)
                paginaAtual = 0;
                EstadoAtualDocumento = true;
                texto = t;
                ProcessarTexto();
                TraducaoPronta(paginasTraducao.Count);
            }

            private void ProcessarTexto() {
                paginasTraducao = new List<IPagina>();
                ValidarCarateres();
                switch (texto.Texto) {
                    case "Olá":
                        paginasTraducao.Add(new Pagina("\nNǐ\n"));
                        paginasTraducao.Add(new Pagina("\nhǎo\n"));
                        break;
                    case "Bem-vindo":
                        paginasTraducao.Add(new Pagina("\nHuānyíng\n"));
                        paginasTraducao.Add(new Pagina("\nnǐ\n"));
                        break;
                    default:
                        paginasTraducao.Add(new Pagina("Ainda não sei traduzir isso."));
                        break;
                }
            }

            private IDictionary<char,int> ObterCarateresInvalidos(ITexto textoAanalisar) {
                Dictionary<char, int> carateresInvalidosEncontrados = new Dictionary<char, int>();
                foreach (char c in listaCaracteresInvalidos)
                {
                    if (textoAanalisar.Texto.Contains("" + c))
                    {
                        if (!carateresInvalidosEncontrados.ContainsKey(c))
                            carateresInvalidosEncontrados.Add(c, 0);
                        carateresInvalidosEncontrados[c]++;

                    }
                }
                return carateresInvalidosEncontrados;
            }

            private void ValidarCarateres() {
                IDictionary<char,int> carateresEncontrados = ObterCarateresInvalidos(texto);
                if (carateresEncontrados.Count>0) {
                    throw new ExceptionCaracteresInvalidos(carateresEncontrados);
                }
            }

            public int SolicitarPaginaAtual(ref IPagina pagina) {
                // Fornece o texto traduzido da página atual
                // colocando-o em "pagina" porque foi passada com ref
                pagina.Texto = paginasTraducao[paginaAtual].Texto;
                return paginaAtual;
            }
            public void AvancarPagina() {
                // Avança para a próxima página traduzida
                paginaAtual++;
                if (paginaAtual >= paginasTraducao.Count) EstadoAtualDocumento = false;
                // estadoAtualDocumento regista sucesso (true) ou fim (false)
                if (EstadoAtualDocumento)
                    PaginaAtualMudou(EstadoAtualDocumento);
                else
                    DocumentoTerminou();
            }

            public ITexto CorrigirCarateresInvalidos(ITexto textoAcorrigir) {
                string textocorrigido = textoAcorrigir.Texto;
                IDictionary<char,int> carateresAeliminar = ObterCarateresInvalidos(textoAcorrigir) as IDictionary<char,int>;
                foreach(char c in textoAcorrigir.Texto)
                    if(!carateresAeliminar.ContainsKey(c))
                        textocorrigido += c;
                return new TextoGenerico(textocorrigido);
            }
        }
    }
}