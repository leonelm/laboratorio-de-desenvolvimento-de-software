﻿using System;
using System.Collections.Generic;

namespace Quarto_Chines {
    namespace Model {
        internal class ExceptionCaracteresInvalidos : Exception {
            IDictionary<char,int> carateres;
            public ExceptionCaracteresInvalidos() {
            }

            public ExceptionCaracteresInvalidos(IDictionary<char,int> message) : base (message.ToString()) {
                carateres = message;
            }

            public IDictionary<char,int> Carateres
            {
                get { return carateres; }
            }
        }
    }
}